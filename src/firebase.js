import { initializeApp } from "firebase/app"
import { getFirestore } from "firebase/firestore"

// your web configuration firebase

const firebaseConfig = {
    apiKey: "AIzaSyDyUPZD7t_xY5Nd_pWe_Bw5WzsA5fzgieI",
    authDomain: "todo-crud-7aaf7.firebaseapp.com",
    projectId: "todo-crud-7aaf7",
    storageBucket: "todo-crud-7aaf7.appspot.com",
    messagingSenderId: "269268691433",
    appId: "1:269268691433:web:448c00bcdac2789f28c378"
  };

  const app = initializeApp(firebaseConfig);
  const db = getFirestore(app);

  export { db };